「老爺，終於到地方了」

之後，我們再次啟程，沿著正確的道路來到了村子
雖然還擔心會不會被帶到什麼莫名其妙的地方去，但這次什麼也沒有發生的到達了目的地盧莎村

亞蘇爾一路上也沒有什麼奇怪的表現，看起來就只是個普通旅行商人的樣子，想不出他都準備了什麼陰謀
他作為「哥布林」的手段是不是已經用盡了呢
嘛，叫來了很多搗蛋哥布林，還下了安眠藥，擔心被懷疑才會暫時停手的吧

雖然在我們這邊看來，已經完全暴露了
順便一提，安眠藥對我來說完全無效，羅蕾露和奧格利也差不多是那樣
羅蕾露提前用了魔術解毒，奧格利也悄悄採用了一些手段，把毒性消掉了

雖然不知道具體是怎麼做到的，但奧格利畢竟是個銀級冒険者
這種程度的詭計，一下子就能看穿並擊破了

「啊，終於到了。太好了啊，迷路的時候我還有些不安，不知道會怎麼樣呢」
「老爺，真是一言難盡啊⋯⋯」

亞蘇爾露出有些沮喪的表情，再次表示了歉意
這也是演技吧，感覺他也算個不錯的演員
比起當特工，找間劇院當演員還比較好吧？
雖然說不上英俊小生，但表情和氣氛都很獨特，現在雖然一副普通模樣，但是昨晚和同伴說話的時候，也能感受到相當的存在感
還有就是發聲什麼的，不過那邊不試試看的話也不清楚怎樣

「哈哈哈，嘛，到了這裡，應該就沒問題了。我們要休整一下，亞蘇爾就專心處理車夫的工作吧」
「哦」

亞蘇爾這樣應了一聲，拉上幌子，讓馬車動了起來
話雖如此，剩下的就是找間旅館把馬車停過去了
應該沒必要擔心吧

「⋯⋯有點出乎意料呢，什麼也沒發生」

幌子被拉上後，羅蕾露這樣說到

「是啊，還以為路上一定會發生什麼呢⋯⋯應該是之後才會來吧」

奧格利點了點頭，但沒有放鬆戒備

「會在村子裡嗎⋯⋯雖然在住宿的地方想放鬆一下，但這次看來不行了呢」

這副身體雖然很少會疲勞，但精神上就要另當別論了
長時間不睡覺就會積累起倦怠感，雖然適當放鬆一下也能起到一定效果，但也不能一直這樣

「沒辦法，不過滯留期間本來就會很無聊，正好用來打發打發時間吧。當然也不能過分小瞧了對方」

羅蕾露這樣說到，感覺她是不是太積極了點


◆◇◆◇◆

盧莎村是亞蘭王国數量繁多的村子之一，而且是小到很容易忽略掉的那一類

雖然不像我的故鄉那樣偏僻，但應該也沒有藏著什麼內情
是一個非常平靜的小村子
相比之下，甚至讓人產生一種馬路特是大城市的錯覺
村子裡除了農民，還有幾個獵人和木匠。有商人在村裡開了一間販賣雜貨的小店
除此之外就是村民們偶爾會去喝上一杯的小酒館

「很少有冒険者會來這裡，雖然能買到的東西種類很少，但只有酒可以讓你們喝到昏過去」

說出這話的是酒館的店主
他是個虎背熊腰的大漢，但比起壓迫感，更讓人感到一種包容力
其他客人告訴我們，其實他比看上去膽小的多

「雖然不打算喝到昏過去，但還是享受一杯吧⋯⋯對了，我想打聽下這附近的彼得利瑪湖的事⋯⋯」

我這樣說到，店主回答

「彼得利瑪湖麼，你們是為了那個而來的啊。我們平常也不會靠近那裡⋯⋯不過也有熟悉情況的人在。喂，菲麗絲！這邊的人說想打聽湖的事啊」

聽到店主的聲音，店內側的一桌上正在享用料理和甜酒的三個女人中，有一人站了起來

是三個人中看上去最樸素的一位，性格很內向的樣子，雖然站起了身，但還是戰戰兢兢的
在另外兩人的催促下，她慌慌張張來到了這邊

在這種鄉下村子裡，突然來了冒険者，然後自己還被他們叫了過去，真是可怕
畢竟，冒険這裡有很多粗人，在酒館裡喝醉了酒就開始耍酒瘋的人也不少
若這種傢伙纏上了村裡的婦女，事情會如何發展就不言而喻了

不過實際上，這裡也存在著機會
有時候僅僅是幫著斟酒，就能拿到對村民來說與年收入相當的金幣，甚至還有一見鍾情，提出交往請求的人
另外兩個女人會催促她，裡面也有這層意思吧

而且，我們的小隊裡還有羅蕾露這個女性在，所以她們也許認為我們不會做出什麼無禮之事吧
雖然有的人不管小隊裡有沒有女性都會亂來就是了⋯⋯

「那，那個。我⋯⋯」

結結巴巴的，是在緊張吧
羅蕾露對菲麗絲露出了微笑

「啊，不用把肩膀綳得那麼緊，我們不會做什麼壊事啦，只是想問你幾個問題。明天我們打算前往這附近的彼得馬利湖，在那之前，想要了解一下湖附近的地形還有距離，以及湖本身的面積和生物分佈。店主說你很熟悉湖的事情，所以才叫你過來的」