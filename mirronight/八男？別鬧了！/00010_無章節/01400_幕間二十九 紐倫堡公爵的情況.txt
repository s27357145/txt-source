﻿幕間二十九 紐倫堡公爵的情況

1-1.

「紐倫堡公爵，閣下終於要滅亡了嗎？

無謀的叛亂和無用的殺戮盡頭，那樣的惡公會傳傳到後世的。閣下作為歷史上的大壞蛋，會讓後世的歷史學者和小說家很喜愛的吧」

「閉嘴，魔族。你也已經活了一千年了是打算收集我的壞話嗎？」

「紐倫堡公爵您似乎沒好好做好功課。雖說是魔族也沒辦法活那麼長的。吾輩今年是一百八十七歳。已經是活超過平均壽命一百年了」

「哼！ 光能活這麼久就該滿足了」

我，紐倫堡公爵如今正面臨滅亡之際。

家臣和服從的貴族還有人，要放棄還太早，也有很多考量過的人也認為只要在我的南部地域的勢力範圍內進行防守戰的話要生存下來是很容易的。

首先是防範帝國軍的進攻，其次活下來策畫和赫爾姆特王國的軍事同盟。

這種想法，似乎有人已經私下派遣使徒過去了。

赫爾姆特王國也沒有太多賢明之人，種種思維與作法都是靠貴族的派閥來運作的。

以和帝國合併為目標，和我方同盟的勢力也應該會動起來才對。(註:這裡有暗示先遣隊的派出，有黑吃黑的嫌疑在)

為此，赫爾姆特王國方面兵力的動向感覺比預料中的要來的少。

要他們突然就與消滅掉先遣隊的我聯手即使會感到厭惡，因和我同盟的貴族提案的關係才讓王國的停頓下來也說不定。

關於這點，是這個草率的同盟案引起的關係。

但是，和赫爾姆特王國同盟是不可能的。

在統一帝國的過程中如果讓外國勢力介入進來的話，要統一大陸就更不可能了。

「終究，如果統一帝國的目標不是獨力完成的話掀起叛亂就沒有意義了」

「這種無用的自尊，會縮短閣下的壽命啊」

「那會如你所料嗎？」

真氣人，相當高的機率會朝那個方向發展吧。

照這種情況推展下去，我會死的。

想起來，打從發動叛亂的一開始就有種種錯誤發生了。

首先是泰蕾莎，沒把妳給殺了。

其他的選帝侯和他們的繼承人，就只有傻瓜才會和妳競爭。

也有生下來是女性的不利點，不過，妳卻有能成為皇帝的才幹。

因為是女人所以也有稍微天真的地方在，那個讓家臣來彌補就行了。

想要把那時候的妳給殺了的時候，卻有個傢伙來阻止了。

即使在赫爾姆特王國也有著年輕首屈一指的魔法使評價的鮑麥斯特伯爵，這傢伙救了身處絕境之中的泰蕾莎。

媽的，被那臭小子攪亂了。

暗示會給賞賜，那笨蛋四兄弟而做了預料外的事情造成失分，泰蕾莎還真是運氣好。

結果，因為泰蕾莎逃了所以才會陷入了讓她組成解放軍的困境。

和解放軍的戰鬥中，出現了超乎必要之外的犧牲。

雖然沒用的大量笨蛋貴族也被處分掉了，但是其他方面的犧牲太多了。

特別是魔法使的大量犧牲。

地下遺蹟的發掘品的損害，以及暫時放棄帝都使其兩虎相爭之計被突破了。

倒是，說那個笨蛋皇帝是虎就言過其實了，不如說是一隻野貓才比較貼切。

野貓和雌虎如照計畫那般分裂了，不過只有野貓這邊派遣遠征軍朝我們這裡過來，跟我預料的一樣光評價下跌就敗死了。

到這裡都還好好地照計畫走……。

「歴史ーーー，有時會誕生出意料外的英雄ーーー」

「吵死了，魔族」

野貓的三男，名叫彼得的臭小子出現了。

由平民的母親所生下，和她的母親很相配淨是在帝都玩樂的小夥子。

帶領同樣是出生背景的雜種狗到處閒晃，就連我發兵當天都還外出到帝都之外去。

不久之後就回到帝都，不過，一臉醉相，大半的家臣都逃了，還敢我的軟禁命令下前去娼館撒嬌了。

在許可的情況下，雖然有監視但並不介意繼續玩他的。

而且，帳款還是向皇家請款。

認為他和優等生的二個哥哥不同只是個無可救藥的臭小子，居然是在玩欺騙我的花招。

而且，這臭小子還和與泰蕾莎保持距離的鮑麥斯特伯爵搭上了。

在兩者保持距離的時間點，我還以為我會贏。

因為鮑麥斯特伯爵和野貓的相性很差，所以我才沒料到他會去幫助他的兒子。

結果，彼得在野貓死後揭義起來迅速地掌握了帝都中樞的政權，還在我大軍壓境至帝都前整頓好軍備，使得我的精銳第一次受到了巨大的損害。

連負責別動隊的心腹紹肯都被討伐了。

從少數逃回來的士兵那裡聽到，他是被鮑麥斯特伯爵用魔法斬-首的。

那個臭小子又再次來妨礙我了。

明明紹肯是我的臂膀才放心將別動隊交給他的，失去那樣的他後要調兵遣將就很困難了。

戰術的運用也跟著變窄了。

2.

「鮑麥斯特伯爵啊。別作怪啊。魔族，那臭小和你們的血脈有關嗎？」

那個臭小子，也是王國的最終兵器吧。

很懷疑那小子，真的是不是普通人了。

「魔族和人間的混血兒已經從這塊大陸消失近一萬年了，有可能是返祖現象」

「返祖現象？」

「很久以前滅亡了，被閣下你們稱作古代魔法文明的那個時代」

「說明一下」

魔族好像知道什麼，我要求進行說明。

現在，無論是什麼情報都想要得到。

「古代魔法文明時代魔法技術有著高度的發展，我們魔族之國也有與他們進行交易」

「說起來，你是從遙遠的西方過來的吧」

外表穿著白色燕尾服頭上還戴著大禮貌、單邊眼鏡和小鬍子等特徵是隨處可見的中年紳士造型，但是魔族的耳朵很長。

那傢伙，突然就在我面前展現了那副耳朵。

推斷最初是要讓我墮落，如果是那樣就是那傢伙故事讀過頭而腦子變傻了。

據說，他是一位考古學者。

『紐倫堡公爵。閣下您的領地內有著大量貴重的古代遺產，所以希望能讓吾輩調查一下』

因為他那老實的態度，使我讀不出來這個男人的本性。

位居背後的魔族之國目標是要讓這塊大陸混亂嗎，還是單純的求知慾才來靠近擁有地下遺蹟及土地權限的我呢。

因為後者的結果，讓我入手了大量的古代文明遺產。

會爆炸的魔物型格雷姆、量産型的龍格雷姆，以及妨礙『移動』和『通信』的巨大裝置。

也入手了其他大量的物品，那些東西對發動叛亂非常有用。

『暫且不提數量，並沒有發掘出比想像中還來的珍貴的物品啊』

他本人，對那樣的成果感到不滿了。

魔族……這傢伙還有另個奇怪的一點就是不提自己的本名。

所以我才用魔族來稱呼他，不過，這傢伙也沒有抱過一句過。

雖然看這傢伙不是很順眼，不過，卻被這傢伙在出土品的修理、維護上幫忙了。

據說不是專家是製造不出來的，但若是簡單的修理和維護好像就辦的到。

作為讓這傢伙在紐倫堡公爵領內探索地下遺蹟的謝禮，就是讓他來幫忙格雷姆軍團的運轉。

『你，果然是魔族之國的？』(註:這邊上下兩句要一直到下面才會點出在講什麼)

『吾輩是偷渡出國的，所以那是沒有的事』

雖然沒做太奇怪的事情，但這傢伙擁有龐大的魔力。

『魔族的數量很少。相對的，每個人都是魔法使』

魔族的魔量很驚人。

這傢伙，雖然定期會為阻礙『移動』和『通信』裝置定期補充魔力、進行出土品的修理和維護的同時，卻還是默默地一個人持續在研究。

『但願魔族真的不會攻過來』

『魔族也有形形色色的人。其中，也存在著提案進出這塊大陸的人』

『進出？』

『魔族，已經來到種族的極限了』

這數千年來，魔族好像很難誕生下新生兒。

『少子化。雖說魔族之國位處在西方的島上，不過卻是和亞大陸一樣的廣闊。沒有戰爭，政治也很安定。明明結婚的人增加了，但是出生的孩子卻變少了。出生率眼看就要掉至１．１了。順便一提，紐倫堡公爵領超過４』 (註:出生率が今にも１．１を切り。這一句的意思是一對夫妻頂多生1.1個孩子，而紐倫堡公爵領是4個起跳)

雖然出現了出生率和奇怪的名詞，不過，比起那些還是比較在意魔族的『進出』。

語言相同。

若是那是侵略，一開始就有可能在那件事情上被欺騙了。(註:發掘遺蹟)

也就是說，像帝國的南進論者、王國的北進論者一樣，魔族之國也存在大陸侵略論者。

現在或許人數很少，但如果魔族之國的少子化得到進展的話那樣的意見說不定就會增加了。

不按牌理出牌？

不可能，戰爭會受人類的本能所影響。

討厭戰爭，搞不好遺漏掉的愚民、自己和家人會戰死，沒受傷的話所屬國家或領主會因勝利而喜悅的。

勝利的一方，是會蹂躪、搾取程度較差的敗者的。

自方人數不足，可以把下等的異族、人類當作奴隸來填補，即使出現這種想法也不奇怪。

魔族會為了取回物種的本能，是有可能進攻這塊人類所居住的大陸。

我要統治這塊大陸，就有必要防範這件事。

例如，利用這位魔族。

「回到原本的話題，鮑麥斯特伯爵是反祖嗎？」

「他的魔量，如果用魔族的基準來說的話是在上級下位左右吧。因為還在增加當中，那樣一來以魔族的基準也很高的可能性高於上級中位。現今的人類是不可能的，考量他是古代魔法文明時代人類的反祖比較妥當」 (註:上級の下、上級の中。簡單說就是A+、A、A-的分類法)

「真瞭解啊」

「雖然沒有見過當時的情況，不過，古代魔法文明中有以人工的方式增加魔法使的技術存在」

「什麼！ 有那種技術啊！」

「也在一萬年前就消失了的技術」

不愧是指以考古學者為業的人，魔族對有關古代魔法文明時代的事情很瞭解。(註:這裡的魔族是指恩斯特，若是只整個魔族翻譯時會另外加字近來)

就連魔法使的製造方法，意外地也知道這項技術。

「魔族之國並沒有滅亡，有留下比較多的資料。古代魔法文明在魔法技術上也極端地推進著」

緩慢地和當時的古代魔法文明時代結合了，而成立了聯邦國家。

位處大陸中央的本國很強大，政治上也沒有太多不安存在同意讓各地方的小國家得到某種程度的統治自由。

「特別是，大陸最南端的秋津洲共和國。那裡，統治者是由國民在選舉中選出來的」

「帝國好像也是如此」

「帝國只有議員能投票。秋津洲共和國，只要是十八歲以上的男女不論是誰都可以投票。和現在的魔族之國一樣」

「哼。愚人政治」

讓不瞭解政治的民眾來選出政治家？

在現在的帝國議會裡如果讓愚蠢的議員持續增加，只會讓政治更混亂吧。

3.

「或許，政治要有手段，事實上實行者有能力的話不論怎樣的政治體制都是無所謂的」

「考古學者，你也兼任政治學者嗎？」

「同一所大學裡的前同事說過的話拿來現學現賣。話說回來，隨著魔法技術的發展，必要的魔法使數量就隨之增加了」

但，應該不是忽然間就大量誕生出魔法使才對。

因此，人工魔法使的增加研究就開始了。

「起先是連續的苦戰，不過，終於是找到了。在生物的設計圖中，發現了魔力和魔法相關聯的東西」(註:原文的生物設計圖，也可以翻成"生物藍圖"，簡單說就是遺傳基因的機率分配)

「生物設計圖？」

「這項情報原本就是所有生物的起源。出生下來孩子和雙親很相似，就是這個原因」 (註:生まれた子供が両親に似る。這整句不是指外表而是魔法才能)

「等等。遺傳上那樣子是生不出魔法使的。倒不如說(和遺傳)完全沒有關係」

如果靠遺傳就能生下魔法使的話，這個世界的魔法使早就人滿為患了。

「畢竟是很困難的專業話題不過還是簡單地說明一下，那項生物設計圖是極端的隱性遺傳要素」

「隱性遺傳？」

「金髪和黒髪的雙親如果要生小孩的話，到底哪個髮色會比較多？

答案，是黑色頭髮的孩子。也就是說，金髮是隱性遺傳」

原來如此，黑髮是顯性遺傳啊。

那麼，我打壓瑞穗人是正確的。

如果讓那些人的人數增長起來的話，帝國就會被內部侵蝕而滅亡了。

「魔法使的生物設計圖，是極端隱性遺傳基因。雙親都不是的話就不會遺傳」

「喂，就算父母都是魔法使生下來的孩子也不會是魔法使吧」

所以，如果不是這樣的話世上早就都是滿滿的魔法使了。

「還有一項，遺傳要素。這是記載了和『魔力具現化和増殖』有關的資料」

「總之，不符合這些條件就沒辦法生出魔法使嗎？」

「雙親不是魔法使就生不出來。只有單方的話，這塊大陸上的人類不論是誰都擁有一定容量的魔力。魔法使的設計圖，意外地現在很多人類都擁有。只要嘗試去取樣領民們的頭髮進行調查會找到二成。以統計學的來思考，我想大概這樣就是平均值了」

(註:片方だけだと、この大陸の人間ならば誰でも持っている量の魔力のみであるな。魔法使いの設計図は、意外と今の人間でも多數が持っているのであるな。意思就是主角所在的大陸，每個人類多少都有魔力，差別在於能不能掌握魔力的動向，這點不懂可以去看蜘蛛子。另一方面無法發動魔法是因為容量太少，容量太少的例子就像伊娜、露易絲這種，當然這一話有興趣的人可以跳麗莎篇和教師篇裡面有提到)

統計學啊……。

看來，魔族之國各種各樣的技術和學問似乎都很發達。

「也就是說，古代魔法文明開發出人工魔法的製造技術，『魔力具現化和増殖』就是讓生物設計固定下來的技術嗎？」(註:生物設計，只要想像成是優生學的概念就好，這邊不改字的原因是這四個字帶有人為干涉遺傳改掉了意思會差很多)

「就是這樣沒錯。魔族因為沒有必要的技術，也是失落的技術」

很遺憾，如果能早點知道的話，就能增產魔法使了。

「那麼，為什麼你會知道鮑麥斯特伯爵是反祖？」

「過去的資料中有記載。古代魔法文明時代所出現的人工魔法使，他們所有人都擁有魔力。還有，從閣下的情報中有發現。他的配偶全部都是魔法使」

「那會發生什麼是？」

「所有人都是，統計數字太奇怪了。資料上有寫結婚之前明明並不是這樣」(註:指伊娜和露易絲)

的確，這點很奇怪。

但是，原本就有素質，結婚後交合在一起就能讓魔法使的才能具現化的話就沒有想過了吧？

『聖女』和『暴風』以及『破壞魔』的二個人都有中級水準。

雖說十分貴重，因為是中級所以也有姑且沒注意的可能性嗎？

不，年幼時的魔力檢查在王國應該是義務才對。

確實，這點很奇怪。

「另一項，從過去的資料中就能明白了。即使雙方都持有生物設計圖，如果沒有對它持續刺激的話到死都不會注意到有魔法使的才能的」

「怎麼說？」

「現代人，(男女)雙方持有生物設計圖的比例在紐倫堡公爵領的領民裡面占百分之八。這也在平均值內」

「這麼說來，魔法使才會這麼少」

第一，根本沒注意到原因在於，為什麼要配置判別用的水晶在偏僻的農村，那麼做一點意義都沒有。

「判別用的水晶和透過交合具現化出魔法使的機率，和古代魔法文明時代沒有差別」

也就是說，連百分之一都不到。

「明明擁有生物設計圖，卻不具有魔法。最初是透過嚴格的修行來喚醒，不過這麼做效率很差」

如果是靠嚴格的訓練才成為魔法使的話，我紐倫堡公爵家諸侯軍的士兵會出現魔法的使用者也沒什麼好不可思議的。

「和種種的研究交叉比對後的結果，發現另一種生物設計圖，因此有三位準備好的人工魔法使被生下來了。這類人工魔法使有個很大的特徵」

那就是，如果和持有二種生物設計圖的異性生孩子的話，這個孩子幾乎百分之一百會變成魔法使。

其次，對擁有二種生命設計圖卻沒有具現化出魔法才能的人給予刺激使其引發出魔法使的才能，對此魔族做了說明。

「其結果，能讓至今不曾想過自己能使用魔法的人發動魔法，即便是已經能使用魔法的人也能夠引出潛在能力並增加魔力。因此，古代魔法文明時代的魔法使才能不斷地增加」

原來如此，那是與大繁榮緊密相連的啊。

「但是，鮑麥斯特伯爵的反祖……。魔族，喚醒潛在潛在能力的因子是什麼？」

「人間勝過魔族的東西。那就是支撐繁殖力的性慾啊。由於伴隨快感的關係，人類才會因此大量增加人數上也才會壓倒性比魔族還要多」

「撇開那些胡說八道的話。魔族也一樣吧」(註:享受X愛...)

因為和人類的容貌沒有差別，又不是靠下蛋來增加數量的。

那種事情聽都沒聽過。

3.

「或許，政治要有手段，事實上實行者有能力的話不論怎樣的政治體制都是無所謂的」

「考古學者，你也兼任政治學者嗎？」

「同一所大學裡的前同事說過的話拿來現學現賣。話說回來，隨著魔法技術的發展，必要的魔法使數量就隨之增加了」

但，應該不是忽然間就大量誕生出魔法使才對。

因此，人工魔法使的增加研究就開始了。

「起先是連續的苦戰，不過，終於是找到了。在生物的設計圖中，發現了魔力和魔法相關聯的東西」(註:原文的生物設計圖，也可以翻成"生物藍圖"，簡單說就是遺傳基因的機率分配)

「生物設計圖？」

「這項情報原本就是所有生物的起源。出生下來孩子和雙親很相似，就是這個原因」 (註:生まれた子供が両親に似る。這整句不是指外表而是魔法才能)

「等等。遺傳上那樣子是生不出魔法使的。倒不如說(和遺傳)完全沒有關係」

如果靠遺傳就能生下魔法使的話，這個世界的魔法使早就人滿為患了。

「畢竟是很困難的專業話題不過還是簡單地說明一下，那項生物設計圖是極端的隱性遺傳要素」

「隱性遺傳？」

「金髪和黒髪的雙親如果要生小孩的話，到底哪個髮色會比較多？

答案，是黑色頭髮的孩子。也就是說，金髮是隱性遺傳」

原來如此，黑髮是顯性遺傳啊。

那麼，我打壓瑞穗人是正確的。

如果讓那些人的人數增長起來的話，帝國就會被內部侵蝕而滅亡了。

「魔法使的生物設計圖，是極端隱性遺傳基因。雙親都不是的話就不會遺傳」

「喂，就算父母都是魔法使生下來的孩子也不會是魔法使吧」

所以，如果不是這樣的話世上早就都是滿滿的魔法使了。

「還有一項，遺傳要素。這是記載了和『魔力具現化和増殖』有關的資料」

「總之，不符合這些條件就沒辦法生出魔法使嗎？」

「雙親不是魔法使就生不出來。只有單方的話，這塊大陸上的人類不論是誰都擁有一定容量的魔力。魔法使的設計圖，意外地現在很多人類都擁有。只要嘗試去取樣領民們的頭髮進行調查會找到二成。以統計學的來思考，我想大概這樣就是平均值了」

(註:片方だけだと、この大陸の人間ならば誰でも持っている量の魔力のみであるな。魔法使いの設計図は、意外と今の人間でも多數が持っているのであるな。意思就是主角所在的大陸，每個人類多少都有魔力，差別在於能不能掌握魔力的動向，這點不懂可以去看蜘蛛子。另一方面無法發動魔法是因為容量太少，容量太少的例子就像伊娜、露易絲這種，當然這一話有興趣的人可以跳麗莎篇和教師篇裡面有提到)

統計學啊……。

看來，魔族之國各種各樣的技術和學問似乎都很發達。

「也就是說，古代魔法文明開發出人工魔法的製造技術，『魔力具現化和増殖』就是讓生物設計固定下來的技術嗎？」(註:生物設計，只要想像成是優生學的概念就好，這邊不改字的原因是這四個字帶有人為干涉遺傳改掉了意思會差很多)

「就是這樣沒錯。魔族因為沒有必要的技術，也是失落的技術」

很遺憾，如果能早點知道的話，就能增產魔法使了。

「那麼，為什麼你會知道鮑麥斯特伯爵是反祖？」

「過去的資料中有記載。古代魔法文明時代所出現的人工魔法使，他們所有人都擁有魔力。還有，從閣下的情報中有發現。他的配偶全部都是魔法使」

「那會發生什麼是？」

「所有人都是，統計數字太奇怪了。資料上有寫結婚之前明明並不是這樣」(註:指伊娜和露易絲)

的確，這點很奇怪。

但是，原本就有素質，結婚後交合在一起就能讓魔法使的才能具現化的話就沒有想過了吧？

『聖女』和『暴風』以及『破壞魔』的二個人都有中級水準。

雖說十分貴重，因為是中級所以也有姑且沒注意的可能性嗎？

不，年幼時的魔力檢查在王國應該是義務才對。

確實，這點很奇怪。

「另一項，從過去的資料中就能明白了。即使雙方都持有生物設計圖，如果沒有對它持續刺激的話到死都不會注意到有魔法使的才能的」

「怎麼說？」

「現代人，(男女)雙方持有生物設計圖的比例在紐倫堡公爵領的領民裡面占百分之八。這也在平均值內」

「這麼說來，魔法使才會這麼少」

第一，根本沒注意到原因在於，為什麼要配置判別用的水晶在偏僻的農村，那麼做一點意義都沒有。

「判別用的水晶和透過交合具現化出魔法使的機率，和古代魔法文明時代沒有差別」

也就是說，連百分之一都不到。

「明明擁有生物設計圖，卻不具有魔法。最初是透過嚴格的修行來喚醒，不過這麼做效率很差」

如果是靠嚴格的訓練才成為魔法使的話，我紐倫堡公爵家諸侯軍的士兵會出現魔法的使用者也沒什麼好不可思議的。

「和種種的研究交叉比對後的結果，發現另一種生物設計圖，因此有三位準備好的人工魔法使被生下來了。這類人工魔法使有個很大的特徵」

那就是，如果和持有二種生物設計圖的異性生孩子的話，這個孩子幾乎百分之一百會變成魔法使。

其次，對擁有二種生命設計圖卻沒有具現化出魔法才能的人給予刺激使其引發出魔法使的才能，對此魔族做了說明。

「其結果，能讓至今不曾想過自己能使用魔法的人發動魔法，即便是已經能使用魔法的人也能夠引出潛在能力並增加魔力。因此，古代魔法文明時代的魔法使才能不斷地增加」

原來如此，那是與大繁榮緊密相連的啊。

「但是，鮑麥斯特伯爵的反祖……。魔族，喚醒潛在潛在能力的因子是什麼？」

「人間勝過魔族的東西。那就是支撐繁殖力的性慾啊。由於伴隨快感的關係，人類才會因此大量增加人數上也才會壓倒性比魔族還要多」

「撇開那些胡說八道的話。魔族也一樣吧」(註:享受X愛...)

因為和人類的容貌沒有差別，又不是靠下蛋來增加數量的。

那種事情聽都沒聽過。

4.

「被這麼一說不得不說近幾年來年輕的魔族都是草食系，對戀愛和結婚沒有興趣，也有很多人埋首在興趣和工作上。政府對少子化的政策，總是在做白工」

擁有高度文明明明生活不辛苦，說不定因此對性慾比較淡薄。 (註:原文後半段是生物としての活気，身為生物的活力)

哼，不論哪個國家或種族都在煩惱精-子太多了。

比起這件事……。

「鮑麥斯特伯爵的妻子們原本就擁有遺傳因子了，和他婚後生活使得魔力具現化出來，並且得到了強化。不過，古代魔法文明時代的研究者們也真是悪趣味啊……」

「是嗎？ 明明是增加魔法使最省事的方法。過去也有魔法使被厚待，所以讓想要變成魔法使的女性和想要生下魔法使的女性都會去接近人工魔法使。人工魔法使這邊，則是喜歡的女性多到選不完。可說是男人的夢想吧？」

「是啊」

我也是男人，所以不是不明白那種心情。

「不過，為什麼鮑麥斯特伯爵的遺傳基因會顯現出來？」

「那是謎，調查那個是學者和研究者的工作。只是，看過閣下給的有關鮑麥斯特伯爵的報告書時覺得很奇怪」

「奇怪？ 為什麼？」

關於鮑麥斯特伯爵的事情，我是花了相當多的時間和金錢去調查的。

那樣的男人會成為敵人，還是成為夥伴呢？

不管是哪個，都有必要瞭解對方。

「你是在否定我的調查嗎？」

「有一個地方很奇怪。鮑麥斯特伯爵的魔力具現化的時期。被記載下來的時間是在五∼六歳的時候。這點很奇怪」

「為什麼？」

「人工魔法使的遺傳因子具現化時，在被生下來的當下馬上就會明白具有魔力的」

「那是因為他的老家是在窮鄉僻壤的地方所以沒有調查技術吧」

「被稱作『人工魔法使的彩虹』，出生後不久會發出彩虹色的光是很平常不過的事。甚至連棄嬰都不是，連負責接生的接生婆和家人都沒發現是不可能的」

「也就是說，五∼六歳時的鮑麥斯特伯爵發生過什麼事嗎？

還是說偶然才在那個時期才能才覺醒呢？」

並沒有保證，所有人可以在出生後一定能馬上讓才能覺醒過來的。

「閣下到底有沒有好好聽吾輩所說的話啊？

生物設計圖是天生的，不可能會在後天突然出現的」

原來如此，生物設計圖在出生之時就已經決定好了。

所以，人工魔法使的魔力不可能會突然就覺醒的。

這麼一來，倒底是怎麼回事？

「鮑麥斯特伯爵聽說是父親超過四十歲時生下來的。被高齡產婦的正妻所生下的，不過，也有是受僕人所生卻欺騙是正妻生的可能性」

貴族裡經常會發生。

妻子以外的女人所生的孩子，會假冒是正妻之子。

原來如此，要是這樣的話我就懂了。

如果母親是貧窮的農民之女，卻沒注意到的可能性。

「為求慎重起見問一下，會有生物設計圖突然異變的的情況嗎？」

「不無可能。只是，對現在這塊大陸來說是不可能的」

「現今是不可能？ 怎麼說？」

「若是一萬年前的當時就有可能了」

一萬年前，即是聯合國家也是大陸統一的狀態，已具有壓倒性繁榮自豪的古代魔法文明的中心國家突然間發生了原因不明的毀滅事件。

擁有比現在還優秀的魔法技術，卻在某一天這個國家突然之間就毀滅了。

帝國・王國雙方的歷史・考古學者直到現在都還沒發現是什麼原因造成的，而且至今還是個謎團。

「吾輩們的魔族也不是萬能的關係，雖然古代魔法文明準確的毀滅原因並不清楚，不過，大致上的原因卻是知道的。過去曾派遣過調查團，也有資料殘存下來」

「那件事，還是第一次聽到啊」

至今因為對考古學沒有興趣所以並不會去問，不過，現在已經有這部分的興趣了。

即使聽到都會想岔開話題，有時候突然會對話題感興趣來。

因為是學者所以才反覆無常，還是背後是魔族之國的關係呢。

偶爾會被氣得發昏。

胡亂生氣會傷害到這個魔族的心情，因此要是不願說話的話會很困擾的，只是現在還不擅長好好和他說話。(註:這一句是指紐倫堡公爵的口氣不好，很容易因恩斯特的行為動怒而變的口氣很差，這話對恩斯特的口氣大多如此，粗魯而不加敬語，連疑問句的呢、吧都很少用)

「簡單來說，就是因為傲慢而失敗了」

「失敗了嗎？」

「隨著人工魔法使的增加使得魔力變得可以處理大量的魔力，好像是因實驗什麼巨大的魔導裝置的關係」

「因失敗而產生爆炸啊……」

「之後進過現場的魔族調查團說，好像是發生過可怕的爆炸」

巨大的斷裂就是當時留下來的。

其他連首都和其周邊數百公里都消滅了，統治大陸所有國家的中樞一瞬間消失掉了。

之後的混亂和破滅猜都能猜到。

「把魔力都集中在一處用常識來看是不該有這種想法的，卻因產生的爆炸而散落在大陸各處。塊狀魔力附著的場所，就變成了大陸的痣，或魔物領域了」

也就是說，古代魔法文明時代當時並沒有魔物領域吧。 (註:原文是古代魔法文明時代以前には魔物の領域は無かったというわけか。這個"以前"是指毀滅之前)

「魔物是怎麼回事？」

「正常來說是身體遭到高濃度魔力通過後，生物的生物設計圖產生異變的關係。也有很多會死亡、畸形或是無法生育的情況，但是其中也有存活下來的物種因而繁榮起來的」

那就是魔物的原形。

「領域的首領是？」

「那片領域之中蘊含最多魔力的存在，打倒牠的話附著在那片土地上的過剩魔力就會消失。如此一來，中・小型的魔物將很難在那片土地上生活」

喂喂，兩國許多研究者都不知道的事情卻劈哩啪啦地回答著。

所以對這個魔族不能疏忽大意。

「真博學啊」

「畢竟是專家。最近魔族的年輕人也有很多不學習的」

「對年輕人來說很討厭被那樣發牢騷喔」(註:因為上一句有影射到紐倫堡公爵)

「吾輩，在年輕時也曾被老人家說過同樣的事」

在這樣的時間點上才明白歷史的真實，但是，卻對戰局的轉好並沒有關聯性。

只有我一人知道許多外人所不知道的事，多少因此有了優越感。

「對了，把鮑麥斯特伯爵的事情給忘了。那個男人為什麼會突然出現返祖？」

「返祖現象，或許沒辦法用太過適切的言語來表達。第三生物設計圖，要靠非常困難的黏著技術才能顯現出來。鮑麥斯特伯爵是偶然出現的這才是正確答案吧」

「喂，等等。這很奇怪不是嗎？

古代魔法文明時代裡給下一代的繼承是可以固定下來的吧。那麼為何子孫沒有繼承第三生物設計圖？」

「失落的固定化技術，大約固定數百年就極限了。不使用固定化的話，慢慢地子孫將無法繼承第三遺傳基因」(註:看不懂很正常，我也看好幾次才懂。意思是自然交配下會回到機率的概念，就算父母當時都是魔法使，其中一人還是人工魔法使的後代，會因另一方是自然產生的魔使造成血緣被沖淡，久而久之就無法在生下是魔法使的小孩。所以要防止這種問題出現就要使用基因固定技術來生下100%是魔法使的小孩)

其他，也在古代魔法文明毀滅後就混亂掉了吧。

或許因此讓大陸上大部分的土地變成了魔物領域。

為了拓展可居住的領域，擁有第三生物設計圖的魔法使被大量動員而在和魔物的戰鬥中死掉了。

應該就是這樣吧？

「哼，鮑麥斯特伯爵和他的子孫在今後的數百年會一生平穩吧」

擁有魔法遺傳要素的一族會被赫爾姆特王國重用，期望能讓王國更加發展。

「而且，我在最壞的時機下發起叛亂造成帝國的國力下降了吧」

「歴史上經常會出現。還是不要在意比較好」

「你會說這種話啊？」

這種把人當成小傻瓜一樣的言行，因為真正的學者是對政治不感興趣的，還是在魔族之國的指示下讓我發起叛亂的呢。

總之，搞不懂這個魔族的想法而感到困擾了。

「那麼，要殺掉鮑麥斯特伯爵嗎？」

「當然了」

不會吧，他可是能增加魔法使的貴重棋子啊。

我該附和嗎，還是考量把他抓起來比較有建設性。

而且，也和你們魔族有關。

少子化衰退中？

真的有這麼一回事的證據？

如果要對抗魔族，魔法使要越多越好。

「那麼，你要果斷地去死嗎？」

「我不論到哪都不算是個好人。那就決定掙扎吧」

總之，現在只能如烏龜一樣緊閉不出來防範彼得的攻擊。

或許，有就有能讓那傢伙統治的帝國變糟的可能性。

「這樣的話，要使用那個嗎？」

「嗯。要用。運轉方面沒問題吧？」

「哎呀，只是動起來是沒問題的」

「那麼就準備進行吧。我也要開始準備了」

為了生存而做的準備。

離最初統一大陸的夢已經相當遠了。

但是，現在用殘留的勢力來防備明天是必要的。

無論如何，我都是個不願放棄的壞男人。
